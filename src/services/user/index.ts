import service from "@/utils/http";
import type {ApiPageResBase} from "@/types/Api";
import type {FetchUserListParams, UserModel} from "@/services/user/user.model";

export const userStatusMap = {
  1:{
    text: '正常', status: 'Success'
  },
  2:{
    text: '停用', status: 'Error'
  }

}

export function fetchUserList(params: FetchUserListParams){
  return service.get<ApiPageResBase<UserModel>>('/users',{
    params
  })
}
