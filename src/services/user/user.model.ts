import type {PageQueryParams} from "@/types/Api";

export interface UserModel {
  id: string;
  name: string;
  countryCode: number;
  phone: string;
  status: number;
  powerDiscount: number;
  curSavePower: number;
  remark: string;
  review: number;
  partnerId: number;
  partnerName: string;
  createdAt: number;
}

export type FetchUserListParams =  {
  phone?: number;
  review?: number
} & PageQueryParams
