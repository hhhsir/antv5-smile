import service from "@/utils/http";


export type LoginParams = {
  phone?: string;
  password?: string;
};

export interface LoginApiResponse {
  accessToken: string;
  accessExpiresIn: number;
  refreshToken: string;
  refreshExpiresIn: number;
  loggedInUser: LoggedInUser;
}
export interface LoggedInUser {
  userId: string;
  type: string;
}


/** 登录接口 POST /api/login/account */
export async function login(data: LoginParams){
  return service.post<{data: LoginApiResponse}>('/auth/login',data)
}
