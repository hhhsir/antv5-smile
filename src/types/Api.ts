
export interface ApiPageResBase<T> {
  data: T[];
  page: number;
  per_page: number;
  total: number;
}


export interface PageQueryParams {
  page: number;
  per_page: number;
}
