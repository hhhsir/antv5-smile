import type {FC} from 'react';
import {PageContainer} from "@ant-design/pro-layout";
import UserList from "@/pages/userList/UserList";

const UserListIndex: FC = ()=>{


  return <PageContainer title="客户管理">
    <UserList />
  </PageContainer>
}

export default UserListIndex
