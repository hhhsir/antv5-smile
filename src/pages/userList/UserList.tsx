import type {FC} from "react";
import useSWR from "swr";
import {fetchUserList, userStatusMap} from "@/services/user";
import {useState} from "react";
import type {FetchUserListParams, UserModel} from "@/services/user/user.model";
import ProTable from '@ant-design/pro-table';
import type {ProColumns} from "@ant-design/pro-table";

const UserList: FC = () => {

  const [pageQuery, setPageQuery] = useState<FetchUserListParams>({
    page: 1,
    per_page: 10
  })
  const {data: userList} = useSWR([pageQuery, '/user'], (_pageQuery) => fetchUserList({..._pageQuery}))
  const columns: ProColumns<UserModel>[] = [

    {
      title: '序号',
      dataIndex: 'index',
      valueType: 'index',
    },
    {
      title: '手机号',
      dataIndex: 'phone',
    }, {
      title: '昵称',
      dataIndex: 'name',
    }, {
      title: '账号状态',
      dataIndex: 'status',
      valueEnum:userStatusMap
    }, {
      title: '注册时间',
      dataIndex: 'createdAt',
      valueType: 'date',
    }, {
      title: '当前存力',
      dataIndex: 'curSavePower',
    }, {
      title: '渠道',
      dataIndex: 'powerDiscount',
    }, {
      title: '备注',
      dataIndex: 'remark',
    }, {
      title: '操作',
      dataIndex: 'createdAt',
      valueType: 'date',
    },

  ]
  return <div>
    <ProTable
      columns={columns}
      rowKey="id"
      dataSource={userList?.data?.data}/>
  </div>
}

export default UserList
