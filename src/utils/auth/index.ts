
const tokenKey = 'accessToken'
export function setToken(token: string){
  window.localStorage.setItem(tokenKey,token)
}

export function getToken(){
  return localStorage.getItem(tokenKey)
}
