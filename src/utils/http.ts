import axios from 'axios';
import {message} from "antd";
import {getToken} from "@/utils/auth";
// import { message } from 'antd';
// import { logout } from '@/service/logout';
const service = axios.create({
  baseURL: '/api-v1-admin',
  timeout: 5000, // request timeout
  headers: {
    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,fr;q=0.7',
    'client-type': 'web',
    lang: 'zh',
  },
});
service.interceptors.request.use((config)=>{
  const token =  getToken()
  if(token){
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
})

service.interceptors.response.use(
  (response) => {

    return response;
  },
  (error) => {
    if(error?.response?.data?.message){
      message.error(error?.response?.data?.message)
    }

    console.log(error.response, 'error');
    return Promise.reject(error)
  },
);

export default service;
